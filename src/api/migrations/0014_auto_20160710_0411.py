# -*- coding: utf-8 -*-
# Generated by Django 1.9.3 on 2016-07-10 04:11
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0013_auto_20160710_0354'),
    ]

    operations = [
        migrations.AlterField(
            model_name='horchata',
            name='location',
            field=models.CharField(
                verbose_name='Descripcion pequeña', max_length=140,
                default=''),
        ),
    ]
